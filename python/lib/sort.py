

def merge_sort(arr):
    if len(arr )==1:
        return arr

    mid_index = int(len(arr ) /2)
    arr_sorted1 = merge_sort(arr[:mid_index])
    arr_sorted2 = merge_sort(arr[mid_index:])

    return merge(arr_sorted1, arr_sorted2)

def merge(arr1 , arr2):
    i, j, k = 0 ,0 ,0
    arr_k = []

    while(len(arr1) != 0  or len(arr2) != 0):
        if len(arr1) == 0:
            arr_k.append(arr2[0])
            del arr2[0]
            continue

        if len(arr2) == 0:
            arr_k.append(arr1[0])
            del arr1[0]
            continue

        if arr1[0] < arr2[0]:
            arr_k.append(arr1[0])
            del arr1[0]
        else:
            arr_k.append(arr2[0])
            del arr2[0]

    return arr_k


def quick_sort(arr, start, end):
    #print(start, end)
    if (start >= end):
        return;
    index = partition(arr,start,end)
    quick_sort(arr, start, index-1)
    quick_sort(arr, index+1, end)
    #print(arr)

def partition(arr, start, end) -> int:
    pivot = arr[end]
    index_pivot = start
    index = start
    while(index < end):
        if ( arr[index] < pivot ):
            swap(arr, index, index_pivot)
            index_pivot += 1
        index += 1

    swap(arr, index_pivot, end)
    return index_pivot


def swap(arr, index_a, index_b):
    temp = arr[index_a]
    arr[index_a] = arr[index_b]
    arr[index_b] = temp


