
import random

def create_ran_list(size=10, max=20):
    '''
    Create randomly generated list of size with maximum int max
    '''
    return [random.randint(0,max) for _ in range(max)]