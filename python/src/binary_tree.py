from typing import List

# 94. Binary Tree Inorder Traversal (Medium)
# Given a binary tree, return the inorder traversal of its nodes' values.
#
# Example:
#
# Input: [1,null,2,3]
#    1
#     \
#      2
#     /
#    3
#
# Output: [1,3,2]
# Follow up: Recursive solution is trivial, could you do it iteratively?
#
# Definition for a binary tree node.
class TreeNode:
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None
def inorderTraversal_recurrsive(self, root: TreeNode) -> List[int]:
    '''
    52ms
    13.2MB
    '''
    if root == None:
        return
    
    self.recurrsive(root.left)
    self.list.append(root.val)
    self.recurrsive(root.right)
    return
    
def inorderTraversal_iterative(self, root: TreeNode) -> List[int]:
    '''
    36ms
    13.1MB
    '''
    stack = []
    list_result = []
    curr = root
    
    while(len(stack) != 0 or curr != None):
        while(curr != None):
            stack.append(curr)
            curr = curr.left
        
        curr = stack.pop()
        list_result.append(curr.val)
        curr = curr.right
        
    return list_result

# 101. Symmetric Tree (Easy)
# Given a binary tree, check whether it is a mirror of itself (ie, symmetric around its center).
#
# For example, this binary tree [1,2,2,3,4,4,3] is symmetric:
#
#     1
#    / \
#   2   2
#  / \ / \
# 3  4 4  3
# But the following [1,2,2,null,3,null,3] is not:
#     1
#    / \
#   2   2
#    \   \
#    3    3
# Note:
# Bonus points if you could solve it both recursively and iteratively.
import queue
# def isSymmetric(self, root: TreeNode) -> bool:
#     self.queue = queue.Queue()
#     # return self.recurrsive(root, root)
#     return self.iterative(root)
def isSymmetric_recurrsive(self, root1: TreeNode, root2: TreeNode) -> bool:
    if root1 == None and root2 == None:
        return True
    if root1 == None or root2 == None:
        return False

    if root1.val != root2.val:
        return False

    return (self.recurrsive(root1.left, root2.right) and
            self.recurrsive(root2.left, root1.right))

def isSymmetric_iterative(self, root: TreeNode) -> bool:
    result = True
    self.queue.put(root)
    self.queue.put(root)
    while (not self.queue.empty()):
        root1 = self.queue.get()
        root2 = self.queue.get()

        if root1 == None and root2 == None:
            continue

        if root1 == None or root2 == None:
            return False

        if root1.val != root2.val:
            return False

        self.queue.put(root1.left)
        self.queue.put(root2.right)

        self.queue.put(root2.left)
        self.queue.put(root1.right)
    return result

# 606. Construct String from Binary Tree (Easy)
# You need to construct a string consists of parenthesis and integers from a binary tree with the preorder traversing way.
#
# The null node needs to be represented by empty parenthesis pair "()". And you need to omit all the empty parenthesis pairs that don't affect the one-to-one mapping relationship between the string and the original binary tree.
#
# Example 1:
# Input: Binary tree: [1,2,3,4]
#        1
#      /   \
#     2     3
#    /
#   4
#
# Output: "1(2(4))(3)"
#
# Explanation: Originallay it needs to be "1(2(4)())(3()())",
# but you need to omit all the unnecessary empty parenthesis pairs.
# And it will be "1(2(4))(3)".
# Example 2:
# Input: Binary tree: [1,2,3,null,4]
#        1
#      /   \
#     2     3
#      \
#       4
#
# Output: "1(2()(4))(3)"
#
# Explanation: Almost the same as the first example,
# except we can't omit the first parenthesis pair to break the one-to-one mapping relationship between the input and the output.
def tree2str_recursive(self, t: TreeNode):

    if t == None:
        return

    print('t.val: ', t.val)
    self.my_str += str(t.val)

    if t.left == None and t.right:
        self.my_str += '()'
    if t.left:
        print('t.left: ', t.left.val)
        self.my_str += '('
        self.recursive(t.left)
        self.my_str += ')'

        # if t.right == None and t.left:
    #     self.my_str+='()'
    if t.right:
        print('t.right: ', t.right.val)
        self.my_str += '('
        self.recursive(t.right)
        self.my_str += ')'

def tree2str_iterative(self, t: TreeNode):
    stack = []
    result = ''
    stack.append(t)
    while (len(stack) != 0):

        curr = stack.pop()

        if curr == None:
            continue

        if curr == '(':
            result += '('
            continue

        if curr == ')':
            result += ')'
            continue

        result += str(curr.val)

        if curr.right:
            stack.append(')')
            stack.append(curr.right)
            stack.append('(')

        if curr.left == None and curr.right:
            stack.append(')')
            stack.append('(')
            continue

        if curr.left:
            stack.append(')')
            stack.append(curr.left)
            stack.append('(')
    return result



