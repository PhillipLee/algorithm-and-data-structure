import unittest
import sys
sys.path.append('../lib')
from lib import sort

class MyTestCase(unittest.TestCase):
    def test_quick_sort(self):
        arr1 = [1, 2, 4, 5, 6, 8]
        arr2 = [10, 21, 4, 7, 2, 20]
        arr3 = [31, 25, 34, 55, 35, 31]
        arr4 = [10, 21, 443, 777, 2, 205]

        sort.quick_sort(arr1, 0, len(arr1)-1)
        print(arr1)

        sort.quick_sort(arr2, 0, len(arr2) - 1)
        print(arr2)

        sort.quick_sort(arr3, 0, len(arr3) - 1)
        print(arr3)

        sort.quick_sort(arr4, 0, len(arr4) - 1)
        print(arr4)
        #arr1.sort()

        #self.assertEqual(arr1_quick, arr1)



    def test_merge_sort(self):
        arr1 = [1, 2, 4, 5, 6, 8]
        arr2 = [10, 21, 4, 7, 2, 20]
        arr3 = [31, 25, 34, 55, 35, 31]
        arr4 = [10, 21, 443, 777, 2, 205]

        arr1_merge = sort.merge_sort(arr1)
        arr2_merge = sort.merge_sort(arr2)
        arr3_merge = sort.merge_sort(arr3)
        arr4_merge = sort.merge_sort(arr4)
        arr1.sort()
        arr2.sort()
        arr3.sort()
        arr4.sort()
        self.assertEqual(arr1_merge, arr1)
        self.assertEqual(arr2_merge, arr2)
        self.assertEqual(arr3_merge, arr3)
        self.assertEqual(arr4_merge, arr4)



if __name__ == '__main__':
    unittest.main()